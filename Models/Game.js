import mongoose from 'mongoose';
import settings from '../config/settings.js';

const cardSchema = new mongoose.Schema(
  {
    index: { type: Number, min: 0, max: 51, required: [true, `Missing 'cards.index'!`] },
    rank: { type: Number, min: 0, max: 51, required: [true, `Missing 'cards.rank'!`] },
    suit: { type: String, enum: settings.suits, required: [true, `Missing 'cards.suit'!`], trim: true }
  },
  {
    _id: false,
    timestamps: false,
    strict: true
  }
);

const gameSchema = new mongoose.Schema(
  {
    playerOneName: { type: String, required: [true, `Missing field 'playerOneName'`], trim: true },
    playerTwoName: { type: String, required: [true, `Missing field 'playerTwoName'`], trim: true },
    cards: [{ type: cardSchema, required: true }]
  },
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  }
);

gameSchema.method({
  clean: function () {
    const game = this.toObject();
    delete game.cards;
    delete game.__v;

    return game;
  }
});

const Game = mongoose.model('Game', gameSchema);

export default Game;
