import settings from '../config/settings.js';

const { cardNames, suits } = settings;

export default class Card {
  #rank;
  #suit;

  /**
   * @param { number } rank
   * @param { 'spades' | 'hearts' | 'diamonds' | 'clubs' } suit
   */
  constructor (rank, suit) {
    if (typeof rank !== 'number' || rank < 1 || rank > 13) throw new Error(`Invalid parameter 'rank'. Must be number between 1 and 13`);
    if (typeof suit !== 'string' || !suits.includes(suit)) throw new Error(`Invalid parameter 'suit'. Must be ${suits.join(' | ')}`);
    this.#rank = rank;
    this.#suit = suit;
  }

  get rank () {
    return this.#rank;
  }

  get suit () {
    return this.#suit;
  }

  isFaceCard () {
    return this.#rank > 10;
  }

  toString () {
    return `${cardNames[this.#rank] || this.#rank} of ${this.#suit[0].toUpperCase() + this.#suit.slice(1)}`;
  }
}
