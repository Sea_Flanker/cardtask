import Card from './Card.js';
import settings from '../config/settings.js';

export default class Deck {
  /** @type Card[] */
  #cards = [];

  /**
   * @param { {index: number, rank: number, suit: string}[]? } cards
   */
  constructor (cards) {
    if (cards) {
      if (!Array.isArray(cards)) throw new Error(`Invalid parameter 'cards'. Must be array`);
      if (cards.some(card => card.constructor !== Object)) throw new Error(`Invalid element in array 'cards'. All must be objects!`);
      if (cards.some(card => typeof card.index !== 'number' || card.index < 0 || card.index > 51)) throw new Error(`Invalid element in array 'cards'. Missing or invalid field 'index'!`);

      this.#cards = cards.sort((a, b) => a.index - b.index).map(card => new Card(card.rank, card.suit));
    } else {
      settings.suits.forEach(suit => {
        for (let i = 1; i <= 13; i++) {
          this.#cards.push(new Card(parseInt(i), suit));
        }
      });

      this.shuffle();
    }
  }

  get cards () {
    return this.#cards;
  }

  shuffle () {
    if (!this.#cards.length) throw new Error('Deck is empty');

    const shuffledDeck = [];

    while (this.#cards.length > 0) {
      const randomIndex = Math.floor(Math.random() * this.#cards.length);
      shuffledDeck.push(this.#cards[randomIndex]);
      this.#cards.splice(randomIndex, 1);
    }

    this.#cards = shuffledDeck;
  }

  count () {
    return this.#cards.length;
  }

  draw (count) {
    if (typeof count !== 'number' || count < 1) throw new Error(`Missing or invalid param 'count'. Must be number bigger from zero!`);
    if (count > this.#cards.length) throw new Error(`Cannot draw ${count} cards. Deck have only ${this.#cards.length}.`);

    const result = [];
    for (let i = 0; i < count; i++) {
      const elka = this.#cards.pop();
      result.push(elka);
    }
    return result;
  }

  toObject () {
    return this.#cards.map((card, index) => ({ index, rank: card.rank, suit: card.suit }));
  }
}
