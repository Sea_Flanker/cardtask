import express from 'express';
import * as controller from '../controllers/game.js';
const router = express.Router();


router.post('/create', controller.create);
router.get('/shuffle/:gameId', controller.shuffle);
router.put('/draw/:gameId', controller.draw);
router.post('/compare', controller.compare);

export default router;
