import game from './game.js';

/**
 * Main router
 * @param {import('express').Application} app
 */
export default (app) => {
  app.use('/game', game);
  app.all('*', (_, res) => res.status(404).send('404 Not Found!').end());
};
