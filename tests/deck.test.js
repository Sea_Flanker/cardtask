/* eslint-disable no-undef */
import Deck from '../Classes/Deck.js';
import Card from '../Classes/Card.js';

describe('DECK CLASS', () => {
  const incompleteCard = { suit: 'spades', rank: 10 };
  const wrongCard = { suit: 'spades', rank: 14, index: 2 };

  let testDeck;

  test('Call constructor with wrong exist card array', () => {
    expect(() => new Deck([incompleteCard])).toThrow();
    expect(() => new Deck([{ ...incompleteCard, index: 52 }])).toThrow();
    expect(() => new Deck([{ ...incompleteCard, index: -1 }])).toThrow();
    expect(() => new Deck([wrongCard])).toThrow();
  });

  test('Call constructor for new deck', () => {
    expect(() => new Deck()).not.toThrow();
    testDeck = new Deck();
  });

  test('Call constructor with empty array', () => expect(() => new Deck([])).not.toThrow());

  test('Shuffle deck', () => {
    expect(() => testDeck.shuffle()).not.toThrow();
    const emptyDeck = new Deck([]);
    expect(() => emptyDeck.shuffle()).toThrow();
  });

  test('Check count', () => expect(testDeck.count()).toBe(52));

  test('Draw cards', () => {
    expect(() => testDeck.draw()).toThrow();
    expect(() => testDeck.draw(0)).toThrow();
    expect(() => testDeck.draw(55)).toThrow();
    expect(() => testDeck.draw(2)).not.toThrow();

    const fewCards = testDeck.draw(5);
    expect(fewCards).toBeInstanceOf(Array);
    expect(fewCards.every(el => el instanceof Card)).toBe(true);
  });

  test('Check count again', () => expect(testDeck.count()).toBe(45));

  test('Method toObject', () => {
    const obj = testDeck.toObject();
    expect(obj).toBeInstanceOf(Array);
    expect(obj[0]).toHaveProperty('index');
    expect(obj[0]).toHaveProperty('rank');
    expect(obj[0]).toHaveProperty('suit');
  });
});
