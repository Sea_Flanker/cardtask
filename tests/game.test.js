/* eslint-disable no-undef */
import supertest from 'supertest';
import mongoose from 'mongoose';
import express from 'express';
import 'dotenv/config';
import expressConfig from '../config/express.js';
import routes from '../routes/index.js';

const app = express();
expressConfig(app);
routes(app);

const mongoURI = process.env.DEV_DB || '';
const fakeId = '632dec654b58fbc0a4b45f8b';

beforeAll(async () => {
  mongoose.set('strictQuery', true);
  await mongoose.connect(`${mongoURI}?retryWrites=true&w=majority`, { useNewUrlParser: true, useUnifiedTopology: true });
});

afterAll(async () => {
  await mongoose.connection.close();
});


describe('CREATE GAME POST /game/create', () => {
  const address = '/game/create';

  test('Missing player one name', async () => {
    const res = await supertest(app).post(address).send({ none: null });
    expect(res.statusCode).toBe(422);
  });

  test('Missing player two name', async () => {
    const res = await supertest(app).post(address).send({ playerOne: null });
    expect(res.statusCode).toBe(422);
  });

  test('Invalid player name', async () => {
    const res = await supertest(app).post(address).send({ playerOne: '  ', playerTwo: 'John' });
    expect(res.statusCode).toBe(422);
  });

  test('Success create game', async () => {
    const res = await supertest(app).post(address).send({ playerOne: 'Catherine', playerTwo: 'John' });
    await mongoose.models.Game.findOneAndDelete({ _id: res.body.payload._id });

    expect(res.statusCode).toBe(201);
    expect(res.body.payload).toHaveProperty('playerOneName');
    expect(res.body.payload).toHaveProperty('playerTwoName');
  });
});

describe('SHUFFLE DECK GET /game/shuffle/:gameId', () => {
  const address = '/game/shuffle/';

  test('Invalid id', async () => {
    const res = await supertest(app).get(`${address}null`);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('null is not valid Mongodb ID');
  });

  test(`Game don't exists`, async () => {
    const res = await supertest(app).get(`${address}${fakeId}`);
    expect(res.statusCode).toBe(404);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe(`Game with id ${fakeId} don't exists!`);
  });

  test(`Card deck is empty`, async () => {
    const tempGame = await mongoose.models.Game.create({ playerOneName: 'Catherine', playerTwoName: 'John' });

    const res = await supertest(app).get(`${address}${tempGame._id}`);
    expect(res.statusCode).toBe(409);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe(`Card deck is empty!`);

    await mongoose.models.Game.findOneAndDelete({ _id: tempGame._id });
  });

  test(`Success shuffle deck`, async () => {
    const tempGame = await mongoose.models.Game.create({ playerOneName: 'Catherine', playerTwoName: 'John', cards: [{ index: 0, rank: 10, suit: 'spades' }] });

    const res = await supertest(app).get(`${address}${tempGame._id}`);
    expect(res.statusCode).toBe(204);

    await mongoose.models.Game.findOneAndDelete({ _id: tempGame._id });
  });
});


describe('DRAW CARD PUT /game/draw/:gameId', () => {
  const address = '/game/draw/';

  test('Invalid id', async () => {
    const res = await supertest(app).put(`${address}null`);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('null is not valid Mongodb ID');
  });

  test(`Game don't exists`, async () => {
    const res = await supertest(app).put(`${address}${fakeId}`);
    expect(res.statusCode).toBe(404);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe(`Game with id ${fakeId} don't exists!`);
  });

  test(`Card deck is empty`, async () => {
    const tempGame = await mongoose.models.Game.create({ playerOneName: 'Catherine', playerTwoName: 'John' });

    const res = await supertest(app).put(`${address}${tempGame._id}`);
    expect(res.statusCode).toBe(409);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe(`Card deck is empty!`);

    await mongoose.models.Game.findOneAndDelete({ _id: tempGame._id });
  });

  test(`Success draw card`, async () => {
    const tempGame = await mongoose.models.Game.create({ playerOneName: 'Catherine', playerTwoName: 'John', cards: [{ index: 0, rank: 12, suit: 'spades' }] });

    const res = await supertest(app).put(`${address}${tempGame._id}`);
    expect(res.statusCode).toBe(200);
    expect(res.body.success).toBe(true);
    expect(res.body.payload).toHaveProperty('rank');
    expect(res.body.payload).toHaveProperty('suit');
    expect(res.body.payload).toHaveProperty('name');
    expect(res.body.payload?.rank).toBe(12);
    expect(res.body.payload?.suit).toBe('spades');
    expect(res.body.payload?.name).toBe('Queen of Spades');

    await mongoose.models.Game.findOneAndDelete({ _id: tempGame._id });
  });
});

describe('COMPARE CARDS POST /game/compare', () => {
  const address = '/game/compare';

  test('Empty body', async () => {
    const res = await supertest(app).post(address);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('Invalid body! Must be array with min 2 records!');
  });

  test('Wrong body format', async () => {
    const res = await supertest(app).post(address).send({ example: 1 });
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('Invalid body! Must be array with min 2 records!');
  });

  test('Invalid type value in body', async () => {
    const res = await supertest(app).post(address).send(['test 1', 'test 2', 1]);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('Body contain invalid record. All records must be valid strings!');
  });

  test('Duplicates in body', async () => {
    const res = await supertest(app).post(address).send(['test 1', 'test 2', 'test 3', 'test 1']);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('Body contain duplicates! Every card name must be unique!');
  });

  test('Wrong card name', async () => {
    const res = await supertest(app).post(address).send(['King of Spades', 'Jack of Hearts', '1 of Diamonds']);
    expect(res.statusCode).toBe(422);
    expect(res.body.success).toBe(false);
    expect(res.body.message).toBe('Body contain invalid record!');
  });

  test('Compare cards with equal ranks', async () => {
    const res = await supertest(app).post(address).send(['King of Spades', 'King of Hearts']);
    expect(res.statusCode).toBe(200);
    expect(res.body.success).toBe(true);
    expect(res.body.payload === 'King of Hearts and King of Spades have equal ranks' || res.body.payload === 'King of Spades and King of Hearts have equal ranks').toBeTruthy();
  });

  test('Compare cards with different ranks', async () => {
    const res = await supertest(app).post(address).send(['3 of Spades', '10 of Diamonds', 'Queen of Spades']);
    expect(res.statusCode).toEqual(200);
    expect(res.body.success).toBe(true);
    expect(res.body.payload).toBe('Queen of Spades');
  });
});
