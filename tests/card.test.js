/* eslint-disable no-undef */
import Card from '../Classes/Card.js';

describe('CARD CLASS', () => {
  test('Invalid rank', () => expect(() => new Card(14, 'clubs')).toThrow());

  test('Invalid suit', () => expect(() => new Card(14, 'Hearts')).toThrow());

  test('Card is face type', () => {
    const card = new Card(12, 'spades');
    expect(card.isFaceCard()).toBe(true);
  });

  test('Card is NOT face type', () => {
    const card = new Card(10, 'spades');
    expect(card.isFaceCard()).toBe(false);
  });

  test('Card to string', () => {
    const card = new Card(12, 'spades');
    expect(card.toString()).toBe('Queen of Spades');
  });
});
