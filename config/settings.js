import path from 'path';
import 'dotenv/config';


// PATH SETTINGS
const rootPath = path.normalize(path.join(path.resolve(), '/../'));

// SERVER SETTINGS
const tooLongRequest = process.env.TOO_LONG_REQUEST || 20;
const port = process.env.PORT;
const env = process.env.NODE_ENV || 'development';
const suits = ['spades', 'hearts', 'diamonds', 'clubs'];
const cardNames = {
  1: 'Ace',
  11: 'Jack',
  12: 'Queen',
  13: 'King'
};

// DATABASE SETTINGS
const databaseAddress = {
  development: process.env.DEV_DB,
  production: process.env.PROD_DB || ''
};

const settings = {
  rootPath,
  tooLongRequest,
  port,
  db: databaseAddress[env],
  env,
  suits,
  cardNames
};

export default settings;
