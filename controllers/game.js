import * as rest from '../utils/express-helpers.js';
import CError from '../utils/CError.js';
import Game from '../Models/Game.js';
import Deck from '../Classes/Deck.js';
import settings from '../config/settings.js';
import { validString, validateId, compareCards } from '../utils/common-functions.js';


/** @type { import('express').RequestHandler } */
export const create = async (req, res) => {
  try {
    const { playerOne, playerTwo } = req.body;
    if (!validString(playerOne)) throw new CError(`Missing or invalid field 'playerOne'`, 422);
    if (!validString(playerTwo)) throw new CError(`Missing or invalid field 'playerTwo'`, 422);

    const deck = new Deck();
    const result = await Game.create({ playerOneName: playerOne, playerTwoName: playerTwo, cards: deck.toObject() });

    rest.successRes(res, result.clean(), 201);
  } catch (error) {
    rest.errorRes(req, res, error);
  }
};


/** @type { import('express').RequestHandler } */
export const shuffle = async (req, res) => {
  try {
    const { gameId } = req.params;

    if (!validateId(gameId)) throw new CError(`${gameId} is not valid Mongodb ID`, 422);

    const game = await Game.findOne({ _id: gameId }).lean();
    if (!game) throw new CError(`Game with id ${gameId} don't exists!`, 404);

    const deck = new Deck(game.cards);
    if (!deck.count()) throw new CError('Card deck is empty!', 409);

    deck.shuffle();

    await Game.findOneAndUpdate({ _id: gameId }, { cards: deck.toObject() }, { new: false, runValidators: true });

    return res.status(204).end();
  } catch (error) {
    rest.errorRes(req, res, error);
  }
};


/** @type { import('express').RequestHandler } */
export const draw = async (req, res) => {
  try {
    const { gameId } = req.params;

    if (!validateId(gameId)) throw new CError(`${gameId} is not valid Mongodb ID`, 422);

    const game = await Game.findOne({ _id: gameId }).lean();
    if (!game) throw new CError(`Game with id ${gameId} don't exists!`, 404);

    const deck = new Deck(game.cards);
    if (!deck.count()) throw new CError('Card deck is empty!', 409);

    const card = deck.draw(1);

    const result = {
      rank: card[0].rank,
      suit: card[0].suit,
      name: card[0].toString()
    };

    await Game.findOneAndUpdate({ _id: gameId }, { cards: deck.toObject() }, { new: false, runValidators: true });

    rest.successRes(res, result);
  } catch (error) {
    rest.errorRes(req, res, error);
  }
};


/** @type { import('express').RequestHandler } */
export const compare = async (req, res) => {
  try {
    const { body } = req;

    // Check initial body validation
    if (!Array.isArray(body) || body.length < 2) throw new CError('Invalid body! Must be array with min 2 records!', 422);
    if (body.some(record => !validString(record))) throw new CError('Body contain invalid record. All records must be valid strings!', 422);

    // Check for duplicates
    const cleanRecords = body.map(record => record.trim());
    if (new Set(cleanRecords).size !== cleanRecords.length) throw new CError(`Body contain duplicates! Every card name must be unique!`, 422);

    // Check records by regex
    const cardNames = Object.values(settings.cardNames).join('|');
    const suits = settings.suits.map(el => `${el[0].toUpperCase() + el.slice(1)}`).join('|');
    const reg = new RegExp(`^(${cardNames}|[2-9]|10)+ of +(${suits})$`);

    if (cleanRecords.some(el => !reg.test(el))) throw new CError(`Body contain invalid record!`);

    const result = compareCards(cleanRecords);

    rest.successRes(res, result);
  } catch (error) {
    rest.errorRes(req, res, error);
  }
};
