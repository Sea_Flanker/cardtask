import mongoose from 'mongoose';
import settings from '../config/settings.js';


/**
 * Validate string si valid Mongodb ID
 * @param { string } id
 * @returns { boolean }
 */
export const validateId = (id) => mongoose.isValidObjectId(id);


/**
 * Validate string is valid and not empty
 * @param { string } str
 * @returns { boolean }
 */
export const validString = (str) => typeof str === 'string' && str.trim();


/**
 * Get rank from card string
 * @param { string } card
 * @returns { number }
 */
const getCardRank = (card) => {
  const [getRank] = card.split(' ');

  const rank = isNaN(getRank)
    ? Object.keys(settings.cardNames).find(key => settings.cardNames[key] === getRank)
    : getRank;

  return parseInt(rank);
};


/**
 * Compare few card string and return card with hi rank. Compare only ranks not suits!
 * @param { string[] } cards
 * @return { string }
 */
export const compareCards = (cards) => {
  cards = cards.map(card => ({ name: card, rank: getCardRank(card) }));
  const sorted = cards.sort((a, b) => b.rank - a.rank);
  const equals = sorted.filter(x => x.rank === sorted[0].rank);

  return equals.length > 1 ? equals.map(x => x.name).join(' and ') + ' have equal ranks' : sorted[0].name;
};
