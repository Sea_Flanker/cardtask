# CARD GAME BACKEND

## ENVIRONMENT VARIABLES
Variable          | Value       | Description
------------------|-------------|--------------
PORT              | 5000        | Server port
NODE_ENV          | development | Server configurations (in this case only database)
DEV_DB            | ?           | Database URI for development configuration
TOO_LONG_REQUEST  | 20          | Normal time for response in seconds.

## SCRIPTS
- __start__ - runs the current state of the code. Real-time changes do not reflect automatically.
- __dev__ - runs the current state of the code. Takes care of all real-time changes and visualizes them.
- __lint__ - runs ESlint
- __test__ - runs tests with Jest

### GAME ENDPOINTS
Address                     | Description                                     | Method  
----------------------------|-------------------------------------------------|---------
/game/create                | Create new game                                 | POST
/game/shuffle/:gameId       | Shuffle card deck                               | GET
/game/draw/:gameId          | Draw last card from deck                        | PUT
/game/compare               | Compare two or more cards and return hi ranked  | POST

#### /game/create
- Create new game
- Method: POST
- Success status: 201
```JSON
// Example body
{
    "playerOne": "Catherine", // required
    "playerTwo": "John"       // required
}

// Example success result
{
  "success": true,
  "payload": {
    "playerOneName": "Catherine",
    "playerTwoName": "John",
    "_id": "6496aa400f826be23a394428",
    "createdAt": "2023-06-24T08:33:04.023Z",
    "updatedAt": "2023-06-24T08:33:04.023Z",
    "id": "6496aa400f826be23a394428"
  }
}
```

#### /game/shuffle/:gameId
- Shuffle card deck
- Method: GET
- Success status: 204
```JSON
// Example address /game/shuffle/6496aa400f826be23a394428
// The server successfully processed the request, but is not returning any content
```

#### /game/shuffle/:gameId
- Draw last card from deck
- Method: PUT
- Success status: 200
```JSON
// Example address /game/shuffle/6496aa400f826be23a394428

// Example success result
{
  "success": true,
  "payload": {
    "rank": 12,
    "suit": "hearts",
    "name": "Queen of Hearts"
  }
}
```

#### /game/compare
- Compare two or more cards and return hi ranked
- Method: POST
- Success status: 200
```JSON
// Body must be array of min two strings, without duplicates
// Example body
[
    "Ace of Clubs",
    "Ace of Spades"
]

// Example result if body contain cards with equal ranks
{
    "success": true,
    "payload": "Ace of Clubs and Ace of Spades have equal ranks"
}

// Example result if body NOT contain cards with equal ranks
{
    "success": true,
    "payload": "Queen of Clubs"
} 
```